<?
namespace Webcode\Helper;

use Bitrix\Main\Config\Option;
use Webcode\Helper\Settings\AdminSettings;

class Loader
{

    public function IncludeFile(){
        if(Option::get(AdminSettings::ModuleID,checkDebug)) {
           self::LoadDebug();
        }

        if(Option::get(AdminSettings::ModuleID,checkFirebug)) {
            self::LoadFirePHP();
        }
    }

    private static function LoadDebug(){
        if(Option::get(AdminSettings::ModuleID,checkAdmin)) {
            global $USER;
            if($USER->IsAdmin()) {
                require_once $_SERVER['DOCUMENT_ROOT'] . '/local/modules/webcode.helper/Debug/Dump.php';
                require_once $_SERVER['DOCUMENT_ROOT'] . '/local/modules/webcode.helper/Debug/Function.php';
            }
        }
        else {
            require_once $_SERVER['DOCUMENT_ROOT'] . '/local/modules/webcode.helper/Debug/Dump.php';
            require_once $_SERVER['DOCUMENT_ROOT'] . '/local/modules/webcode.helper/Debug/Function.php';
        }
    }

    private static function LoadFirePHP(){

        if(Option::get(AdminSettings::ModuleID,checkAdmin)) {
            global $USER;
            if($USER->IsAdmin()) {
                require_once $_SERVER['DOCUMENT_ROOT'] . '/local/modules/webcode.helper/Debug/FirePHP/vendor/autoload.php';
            }
        }
        else {
            require_once $_SERVER['DOCUMENT_ROOT'] . '/local/modules/webcode.helper/Debug/FirePHP/vendor/autoload.php';
        }
    }

}