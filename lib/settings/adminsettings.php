<?php
namespace Webcode\Helper\Settings;

use Bitrix\Main\Application,
    Webcode\Helper\LangFiles\LangModel;

class AdminSettings {
    const settings = ['tabs'=>[
            [
                'tab_name'=> 'Создание модуля', 'active'=> true,
                'tab_options'=> [
                    ['name'=> "Данные разработчика",'type'=>'line','sort'=>"head"],
                    ['name'=> "Название компании", 'option_str'=> "CompName", 'value'=> "",'type'=>'input'],
                    ['name'=> "Адрес сайта", 'option_str'=> "Site", 'value'=> "",'type'=>'input'],
                    ['name'=> 'Код партнера ', 'option_str'=> "ClientSecret", 'value'=> "",'type'=>'input'],
                    ['name'=> "Данные нового модуля",'type'=>'line','sort'=>"head"],
                    ['name'=> "С админкой?", 'option_str'=> "checkAdminPanel", 'value'=> false,'type'=>'checkbox_n_save'],
                    ['name'=> "Использовать событие prologBefore?", 'option_str'=> "checkOnPrologBefore", 'value'=> false,'type'=>'checkbox_n_save'],
                    ['name'=> "Код модуля (<код партнера> . <код модуля>)", 'option_str'=> "ModuleCode", 'value'=> "",'type'=>'input_n_save'],
                    ['name'=> "Название модуля", 'option_str'=> "ModuleName", 'value'=> "",'type'=>'input_n_save'],
                    ['name'=> "Описание модуля", 'option_str'=> "ModuleDesc", 'value'=> "",'type'=>'textarea_n_save'],
                ],
                'buttons'=>[
                    [
                        'button_name'=>"Создать модуль",'button_function'=>'CreateModule','class'=>'adm-btn-save'
                    ],
                ]
            ],
            [
                'tab_name'=> 'Создание компонента', 'active'=> false,
                'tab_options'=> [

                ],
                'buttons'=>[
                    [
                        'button_name'=>"Создать компонент",'button_function'=>'SaveOptions','class'=>'adm-btn-danger'
                    ]
                ]
            ],
            [
                'tab_name'=> 'Дебаг', 'active'=> false,
                'tab_options'=> [
                    ['name'=> "Только администраторам", 'option_str'=> "checkAdmin", 'value'=> false,'type'=>'checkbox'],
                    ['name'=> "Использовать firebug", 'option_str'=> "checkFirebug", 'value'=> false,'type'=>'checkbox'],
                    ['name'=> "Использовать быстрые функции дебага", 'option_str'=> "checkDebug", 'value'=> false,'type'=>'checkbox'],
                    ['name'=> "Выбор модуля", 'option_str'=> "list", 'value'=> "",'list'=> ['webcode.helper','webcode.hh'],'type'=>'list_n_save'],
                    ['name'=> "Выбор компонента", 'option_str'=> "list", 'value'=> "",'list'=> ['webcode.comp1','webcode.comp2'],'type'=>'list_n_save'],
                    ['name'=> 'p($data) - debug to popup, c($data) - debug to console, f($data) - debug to file ','type'=>'line','sort'=>"head"],
                    ['name'=> 'fb($data) - firePHP. Site - http://www.firephp.org/','type'=>'line','sort'=>"head"],
                ],
                'buttons'=>[
                    [
                        'button_name'=>"Сохранить",'button_function'=>'SaveOptions','class'=>'adm-btn-save'
                    ],
                    [
                        'button_name'=>"Закоментить дебаг",'button_function'=>'SaveOptions','class'=>'adm-btn-save'
                    ],
                    [
                        'button_name'=>"Разкоментить дебаг",'button_function'=>'SaveOptions','class'=>'adm-btn-save'
                    ]
                ]
            ],
            [
                'tab_name'=> 'Редактор lang файлов', 'active'=> false,
                'tab_options'=> [
                    ['name'=> "Выбрать сущьность", 'option_str'=> "EntityList", 'value'=> "",'list'=> ['Модуль','Компонент'],'type'=>'function_list_n_save', 'action'=>'GetListLang'],
                    ['name'=> "Выбрать название сущьности", 'option_str'=> "EntityName", 'value'=> "",'list'=> ['не выбрано'],'type'=>'function_list_n_save', 'action'=>'GetListLang'],
                    ['name'=> "Выбрать файл", 'option_str'=> "FileList", 'value'=> "",'list'=> ['не выбрано'],'type'=>'function_list_n_save', 'action'=>'GetListLang'],
                    ['list'=> [],'type'=>'line','sort'=>"inputs_dinamic","visible" => false],
                ],
                'buttons'=>[
                    [
                        'button_name'=>"Отредактировать файл",'button_function'=>'UpdateLangFile','class'=>'adm-btn-save'
                    ]
                ]
            ],
        ],
    ];
    const TableName ='b_option';
    const ModuleID ='webcode.helper';
    var $setting = false;
    private static $_instance = null;

    private function __construct() {
    }
    protected function __clone() {
    }
    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    private static function GetDBOptions(){
        global $DB;
        $Sql='SELECT `name`,`value` FROM '.self::TableName." WHERE `module_id`='".self::ModuleID."'";
        $res = $DB->Query($Sql, false, $err_mess.__LINE__);
        $setting = self::settings;
        while ($row = $res->Fetch())
        {
            foreach ($setting['tabs'] as $k => $tab) {
                foreach ( $tab['tab_options'] as $index => $option) {
                    if($row['name']===$option['option_str']){
                        if($option['type']=='checkbox')
                            $setting['tabs'][$k]['tab_options'][$index]['value']=($row['value']=='false')?false:true;
                        else
                            $setting['tabs'][$k]['tab_options'][$index]['value']=$row['value'];
                    }
                }
            }
        }
        return $setting;
    }
    public function getSettings() {
        return json_encode(self::GetDBOptions());
    }
}