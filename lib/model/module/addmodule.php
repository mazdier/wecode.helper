<?php

namespace Webcode\Helper\Model\Module;

use Webcode\Helper\Model\AdminModel,
    \Bitrix\Main\IO\File;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

class AddModule
{
    
  public function  CreateModule($data){
      $this->data = $data;

      if(!$this->ValidateData())
        return $this->PrintError();

      AdminModel::SaveOptions($data);

      if(!$this->MakeDir())
        return $this->PrintError();

      if(!$this->CreateFiles())
          return $this->PrintError();

      return $data;
  }

  private function PrintError(){
      fb($this->StrError);
  }

  private function CreateFiles(){
      $ModuleClassName = str_replace('.','_',$this->ModuleID);
      $IncludeClassName = 'C'.preg_replace('/[^a-z]/i','',ucwords(preg_replace('/[^a-z]/i',' ',$this->ModuleID)));
      $NameSpaceModuleLoader = "\\\\".ucfirst($this->Props['PartnerCode'])."\\\\".ucfirst($this->Props['ModuleCode']);
      $this->NewModuleNameSpace = ucfirst($this->Props['PartnerCode'])."\\".ucfirst($this->Props['ModuleCode']);
      $RealPathNewModule = $_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->ModuleID;

      $this->ModulePath = realpath(__DIR__.'/../../../');
      fb([$this->ModulePath]);
      $str = file_get_contents($this->ModulePath.'/Samples/install/index.php');

      $str = str_replace('{MODULE_CLASS_NAME}', $ModuleClassName, $str);
      $str = str_replace('{MODULE_ID}', $this->ModuleID, $str);
      $str = str_replace('{INCLUDE_CLASS_NAME}', $IncludeClassName, $str);

      if(!$this->Props['checkOnPrologBefore']) {
          $str = str_replace('RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "{NAME_SPACE_MODULE_LOADER}\\Loader", "IncludeFile");',
              '', $str);
          $str = str_replace('UnRegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "{NAME_SPACE_MODULE_LOADER}\\Loader", "IncludeFile");',
              '', $str);
      }
      else {
          $str = str_replace('{NAME_SPACE_MODULE_LOADER}', $NameSpaceModuleLoader, $str);
      }

      if(!File::putFileContents($RealPathNewModule.'/install/index.php', $str)) {
          $this->StrError .= "Не удалось создать файл $RealPathNewModule/install/index.php";
          return false;
      }

      $str='<'.'?'."\n".
          '$MESS["'.$this->ModuleID.'_MODULE_NAME"] = "'.EscapePHPString($this->Props['ModuleName']).'";'."\n".
          '$MESS["'.$this->ModuleID.'_MODULE_DESC"] = "'.EscapePHPString($this->Props['ModuleDesc']).'";'."\n".
          '$MESS["'.$this->ModuleID.'_PARTNER_NAME"] = "'.EscapePHPString($this->Props['CompName']).'";'."\n".
          '$MESS["'.$this->ModuleID.'_PARTNER_URI"] = "'.EscapePHPString($this->Props['Site']).'";'."\n".
          '?'.'>';

      if(!File::putFileContents($RealPathNewModule.'/lang/ru/install/index.php', $str)) {
          $this->StrError .= "Не удалось создать файл $RealPathNewModule/lang/ru/install/index.php";
          return false;
      }

      $str = file_get_contents($this->ModulePath.'/Samples/include.php');
      $str = str_replace('{INCLUDE_CLASS_NAME}', $IncludeClassName, $str);
      if(!File::putFileContents($RealPathNewModule.'/include.php', $str)){
          $this->StrError .= "Не удалось создать файл $RealPathNewModule/include.php";
          return false;
      }

      $str = file_get_contents($this->ModulePath.'/Samples/.settings.php');
      $str = str_replace('{MODULE_NAME_SPACE}', $NameSpaceModuleLoader, $str);
      if(!File::putFileContents($RealPathNewModule.'/.settings.php', $str)){
          $this->StrError .= "Не удалось создать файл $RealPathNewModule/.settings.php";
          return false;
      }


      if(!File::putFileContents($RealPathNewModule.'/install/version.php',
          '<'.'?'."\n".
          '$arModuleVersion = array('."\n".
          '	"VERSION" => "1.0.0",'."\n".
          '	"VERSION_DATE" => "'.date('Y-m-d H:i:s').'"'."\n".
          ');'."\n".
          '?'.'>'
            ))
      {
          $this->StrError .= "Не удалось создать файл $RealPathNewModule/install/version.php";
          return false;
      }

      if($this->Props['checkAdminPanel']){
          $this->CopyReplaceFilesAdminPanel();
      }

      if($this->Props['checkOnPrologBefore']){
          $str = file_get_contents($this->ModulePath.'/Samples/lib/loader.php');
          $str = str_replace('{NAME_SPACE}',$this->NewModuleNameSpace,$str);
          if(!File::putFileContents($RealPathNewModule.'/lib/loader.php', $str)){
              $this->StrError .= "Не удалось создать файл $RealPathNewModule/lib/loader.php";
              return false;
          }
      }

      return true;
  }

    private function CopyReplaceFilesAdminPanel(){
        $NewDir = $_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->ModuleID;
        $SamplesDir = $this->ModulePath.'/Samples';

        $arStructure = [
            '/admin',
            '/lib/controller',
            '/lib/model',
            '/lib/settings'
        ];

        foreach($arStructure as $dir) {
            if($handle = opendir($SamplesDir.$dir)){
                while($entry = readdir($handle)){
                    if($entry !== "." && $entry !== ".."){
                        $str = file_get_contents($SamplesDir.$dir.'/'.$entry);
                        $str = str_replace('{NAME_SPACE}',$this->NewModuleNameSpace,$str);
                        $str = str_replace('{MODULE_ID}',$this->ModuleID,$str);
                        $str = str_replace('{MODULE_ACTION_ID}',$this->ModuleActionID,$str);

                        if(!File::putFileContents($NewDir.$dir.'/'.$entry, $str)){
                            $this->StrError .= "Не удалось создать файл $NewDir$dir/$entry";
                            return false;
                        }

                    }
                }
                closedir($handle);
            }
        }
        return true;
    }

  private function MakeDir(){
      $m_dir = $_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->ModuleID;
      $arStructure = [
          $m_dir,
          $m_dir.'/admin',
          $m_dir.'/lang/ru/install',
          $m_dir.'/install',
      ];
      if($this->Props['checkOnPrologBefore'])
          $arStructure[]=$m_dir.'/lib';

      if($this->Props['checkAdminPanel'])
          $arStructure = array_merge($arStructure,[
              $m_dir.'/lib',
              $m_dir.'/lib/controller',
              $m_dir.'/lib/model',
              $m_dir.'/lib/settings'
          ]);
      foreach($arStructure as $dir)
          if (!file_exists($dir) && !mkdir($dir, BX_DIR_PERMISSIONS, true))
              $this->StrError .= "не удалось создать каталог".$dir.'</br>';

     return true;
  }

  private function ValidateData(){
      $this->StrError = false;
      $this->Props = [];

      foreach ($this->data['tab_options'] as $option){
          if($option['type'] !== 'line') {
              switch($option['option_str']) {
                  case "ClientSecret":
                      $match = '#^[a-z][a-z\-0-9]+$#';
                      if (preg_match($match, $option['value']))
                          $this->Props["PartnerCode"] = $option['value'];
                      else
                          $this->StrError .= "Код партнера не соответствует требованиям".'</br>';
                  default:
                      $this->Props[$option['option_str']] = $option['value'];
              }
          }
      }

      $this->ModuleID = $this->Props["PartnerCode"].'.'.$this->Props["ModuleCode"];
      $this->ModuleActionID = $this->Props["PartnerCode"].':'.$this->Props["ModuleCode"];

      if(!empty($this->StrError))
          return false;

      return true;
  }
  
}