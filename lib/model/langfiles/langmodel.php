<?php
namespace Webcode\Helper\Model\LangFiles;

class LangModel
{
    const StrEmptyLangFile = "<??>";
    const Lang = "ru";
    public static function GetListLang($data){
        if(!$path = self::GetPath($data))
            return ['state' => "Выберите сущьность"];
        switch($data[1]){
            case 0:
                fb($path);
                 if($list = self::GetListEntity($path))
                     return ['state' => "Список обновлен", 'index' => 1, 'list' => $list];

            case 1:
                if($data[0]['tab_options'][1]['value']){
                    $ArFiles = self::GetListFilesWithDirectory($path.$data[0]['tab_options'][1]['value']);
                    $NewArFiles=[];
                    if($ArFiles) {
                        foreach ($ArFiles as $arFile) {
                            $NewArFiles[] = str_replace($path.$data[0]['tab_options'][1]['value'],'',$arFile['path']).'/'.$arFile['name'];
                        }
                    }
                    return ['state' => "Список обновлен", 'index' => 2, 'list' => $NewArFiles];
                }

            case 2:
                $ArFile = self::GetLangFile($path,$data[0]['tab_options'][2]['value'],$data[0]['tab_options'][1]['value']);

                return ['state' => "Список обновлен", 'index' => 3, 'list' => $ArFile];

            default:
                return ['state' => "Отредактируйте lang файлы"];
        }
    }

    public static function SetLangFile($data){
        if(isset($data)) {
            if (count($data['tab_options']) >= 3) {
                if (is_array($data['tab_options']['3']['list'])) {
                    foreach ($data['tab_options']['3']['list'] as $key => $val) {
                        static $StrResult = '<?'.PHP_EOL.PHP_EOL, $Prefix;
                        if (isset($val['Name']) && isset($val['Text'])) {
                            if ($key == 0) {
                                if (isset($val['Prefix']))
                                    $Prefix = $val['Prefix'];
                                else return ['state' => "Заполните поля 2"];
                            }
                            $StrResult .= '$MESS["' . $Prefix . '_' . $val['Name'] . '"] = "' . $val['Text'] .'";'. PHP_EOL;
                        }
                    }
                    $StrResult .= PHP_EOL.'?>';
                    if(!$path = self::GetPath([$data]))
                        return ['state' => "Выберите сущьность"];
                    fb($path.$data['tab_options'][1]['value'].'/lang/'.self::Lang.$data['tab_options'][2]['value']);
                    \Bitrix\Main\IO\File::putFileContents($path.$data['tab_options'][1]['value'].'/lang/'.self::Lang.$data['tab_options'][2]['value'],$StrResult);
                    return ['state' => "Lang файл сохранен"];
                }
            } else return ['state' => "Заполните поля"];
        }
        else return ['state' => "Заполните поля"];
    }

    private static function GetPath($data){
        $path='';
        if($data[0]['tab_options'][0]['value']){
            switch ($data[0]['tab_options'][0]['value']){
                case "Модуль":
                    $path = $_SERVER['DOCUMENT_ROOT'].'/local/modules/';
                    break;
                case "Компонент":
                    $path = $_SERVER['DOCUMENT_ROOT'].'/local/components/';
                    break;
            }
            return $path;
        }
        return false;
    }

    private static function GetLangFile($Path,$FilePath,$Name){
        if (file_exists($Path.$Name.'/lang/'.self::Lang.$FilePath)) {
            $file = @fopen($Path.$Name.'/lang/'.self::Lang.$FilePath, "r");
            if ($file) {
                while (($buffer = fgets($file, 4096)) !== false) {
                    if(preg_match('/^\$MESS\["(.+?)"]/is', $buffer, $matches)){
                        $Prefix = strtoupper(str_replace('.','_',$Name));
                        preg_match('/=(.+?);/is', $buffer, $match);
                        $ArFile[] = [
                            'Prefix'=>$Prefix,
                            'Name'=>str_replace($Prefix.'_','',$matches[1]),
                            'Text'=>str_replace('"','',trim($match[1]))
                        ];
                    }
                }
                if (!feof($file)) {
                    fb('fget error');
                }
                fclose($file);
            }
            if($ArFile)
                return $ArFile;
            else return false;

        } else {
            \Bitrix\Main\IO\File::putFileContents($Path.'/lang/'.self::Lang.$FilePath,self::StrEmptyLangFile);
            return false;
        }
    }

    private static function UpdateList($List,$index){
        return ['state' => "Список обновлен", 'index' => $index + 1, 'list' => $List];
    }

    private static function GetListFilesWithDirectory($dir,$ArFiles = []){
        $ArScanDir = self::ScanDirectory($dir);
        if($ArScanDir)
            foreach ($ArScanDir as $file){
                if(is_dir($dir.'/'.$file) && $file != 'lang' && $file != 'vendor' && $file != 'composer' && preg_match('/^[a-z]./', $file)){
                    $ArFiles = self::GetListFilesWithDirectory($dir.'/'.$file, $ArFiles);

                }
                else {
                    if(preg_match('/^\w+\.php/', $file)) {
                        $ArFiles[] = ['path' => $dir, 'name' => $file];
                    }
                }
            }
        return $ArFiles;
    }

    private function ScanDirectory($dir)
    {
        $list = scandir($dir,$sort = 0);
        if (!$list) return false;
        if ($sort == 0) unset($list[0],$list[1]);
        else unset($list[count($list)-1], $list[count($list)-1]);
        return $list;
    }

    private static function GetListEntity($Path){
        $ArDir = [];
        if($ArDirAndFiles = self::ScanDirectory($Path)) {
            foreach ($ArDirAndFiles as $NameDirAndFiles) {
                if (is_dir($Path . $NameDirAndFiles))
                    $ArDir[] = $NameDirAndFiles;
            }
            return $ArDir;
        }
        else return false;
    }
}