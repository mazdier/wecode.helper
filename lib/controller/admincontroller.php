<?php
namespace Webcode\Helper\Controller;

use Bitrix\Main\Engine\Controller,
    \Webcode\Helper\Model\AdminModel,
    \Webcode\Helper\Model\Module\AddModule,
    \Webcode\Helper\Model\LangFiles\LangModel;

class AdminController extends Controller
{
    public function configureActions()
    {
        return [
            'GetListLang' => [
                'prefilters' => []
            ],
            'SaveOptions' => [
                'prefilters' => []
            ],
            'CreateModule' => [
                'prefilters' => []
            ],
            'SetLangFile' => [
                'prefilters' => []
            ],
        ];
    }
    public static function GetListLangAction( $data = [])
    {
        return  LangModel::GetListLang($data);
    }

    public static function SetLangFileAction( $data = [])
    {
        return  LangModel::SetLangFile($data);
    }

    public static function SaveOptionsAction( $data = [])
    {
        return  (new AdminModel)->SaveOptions($data);
    }

    public static function CreateModuleAction( $data = [])
    {
        return  (new AddModule)->CreateModule($data);
    }
}