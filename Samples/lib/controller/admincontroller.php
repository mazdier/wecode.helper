<?php
namespace {NAME_SPACE}\Controller;

use Bitrix\Main\Engine\Controller,
    {NAME_SPACE}\Model\AdminModel;

class AdminController extends Controller
{
    public function configureActions()
    {
        return [
            'SaveOptions' => [
                'prefilters' => []
            ]
        ];
    }

    public static function SaveOptionsAction( $data = [])
    {
        return  (new AdminModel)->SaveOptions($data);
    }
}