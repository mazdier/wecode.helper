<?php

return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '{MODULE_NAME_SPACE}\\Controller' => 'api'
            ],
            'defaultNamespace' => '{MODULE_NAME_SPACE}\\Controller',
        ],
        'readonly' => true
    ]
];
