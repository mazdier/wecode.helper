<?php

use Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

class {MODULE_CLASS_NAME} extends CModule
{

    var $MODULE_ID,
		$MODULE_VERSION,
		$MODULE_VERSION_DATE,
		$MODULE_NAME,
		$MODULE_DESCRIPTION,
		$PARTNER_NAME,
	 	$PARTNER_URI;

	public function __construct(){
    $arModuleVersion=array();
    include(__DIR__."/version.php");
    $this->MODULE_ID = '{MODULE_ID}';
    $this->MODULE_VERSION = $arModuleVersion["VERSION"];
    $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
    $this->MODULE_NAME = Loc::GetMessage("{MODULE_ID}_MODULE_NAME");
    $this->MODULE_DESCRIPTION = Loc::GetMessage("{MODULE_ID}_MODULE_DESC");
    $this->PARTNER_NAME = Loc::GetMessage("{MODULE_ID}_PARTNER_NAME");
    $this->PARTNER_URI = Loc::GetMessage("{MODULE_ID}_PARTNER_URI");
}

	function isVersionD7(){
        return CheckVersion( \Bitrix\Main\ModuleManager::getVersion('main'),'14.00.00');

    }

	function InstallEvents(){
        RegisterModule($this->MODULE_ID);
        RegisterModuleDependences('main', 'OnBuildGlobalMenu', $this->MODULE_ID, '{INCLUDE_CLASS_NAME}', 'OnBuildGlobalMenu');
        RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "{NAME_SPACE_MODULE_LOADER}", "IncludeFile");
        return true;
    }

	function UnInstallEvents(){
        UnRegisterModule($this->MODULE_ID);
        UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', $this->MODULE_ID, '{INCLUDE_CLASS_NAME}', 'OnBuildGlobalMenu');
        UnRegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "{NAME_SPACE_MODULE_LOADER}\\Loader", "IncludeFile");
        return true;
    }

	function InstallFiles(){
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/admin'))
        {
            if ($dir = opendir($p))
            {
                while (false !== $item = readdir($dir))
                {
                    if ($item == '..' || $item == '.' || $item == 'menu.php')
                        continue;
                    file_put_contents($file = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$this->MODULE_ID.'_'.$item,
                        '<'.'? require($_SERVER["DOCUMENT_ROOT"]."/local/modules/'.$this->MODULE_ID.'/admin/'.$item.'");?'.'>');
                }
                closedir($dir);
            }
        }
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->MODULE_ID.'/install/files')){
            CopyDirFiles(__DIR__ . "/files",
                $_SERVER["DOCUMENT_ROOT"]."/local", true, true);
            return true;
        }
    }

	function UnInstallFiles(){
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->MODULE_ID.'/admin')){
            if ($dir = opendir($p)){
                while (false !== $item = readdir($dir)){
                    if ($item == '..' || $item == '.')
                        continue;
                    unlink($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$this->MODULE_ID.'_'.$item);
                }
                closedir($dir);
            }
        }
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->MODULE_ID.'/install/files')){
            DeleteDirFiles(__DIR__ . "/files",
                $_SERVER["DOCUMENT_ROOT"]."/local", true, true);
            return true;
        }
    }

	public function doInstall(){
    $this->InstallFiles();
    $this->InstallEvents();
}

	public function doUninstall(){
    $this->UnInstallFiles();
    $this->UnInstallEvents();
}

}
?>