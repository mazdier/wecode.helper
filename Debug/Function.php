<?php
// Function aliases for fast call

/**
 * Short call function toPopup()
 *
 * @param string $data
 * @param string || null $label
 */
function p($data = '', $label = null)
{
    \Extra\Dump::toPopup($data, $label);
}

/**
 * Short call function toFile()
 *
 * @param $data
 * @param string || null $fileName
 * @param string || null $label
 */
function f($data, $fileName = null, $label = null)
{
    \Extra\Dump::toFile($data, $fileName, $label);
}

/**
 * Short call function dump()
 *
 * @param $data
 */
function d($data)
{
    \Extra\Dump::dump($data);
}

/**
 * Short call function toConsole()
 *
 * @param $data
 * @param string $label
 */
function c($data, $label = '')
{
    \Extra\Dump::toConsole($data, $label);
}

/**
 * Dump and die
 *
 * @param $data
 */
function dd($data)
{
    \Extra\Dump::dump($data);
    die();
}