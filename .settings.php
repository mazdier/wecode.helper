<?php

return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\\Webcode\\Helper\\Controller' => 'api'
            ],
            'defaultNamespace' => '\\Webcode\\Helper\\Controller',
        ],
        'readonly' => true
    ]
];
